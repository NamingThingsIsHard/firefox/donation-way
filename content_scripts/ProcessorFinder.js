/**
 * Describes the result of a processor found in the body.
 *
 * @typedef BodyProcessorInfo
 * @field processorID {String}
 * @field resultID {String}
 * @field node {Node}
 * @field url {URL}
 */


/**
 *
 * @author ubuntu
 */
class ProcessorFinder {
  constructor() {

  }

  /**
   * Finds a processor in the meta
   *
   * @return {Promise<URL>}
   */
  async byMeta() {
    throw "Not implemented"
  }

  _getMetaTag() {
    return document.querySelector(`head meta[name="donation:${this.constructor.ID}"]`)
  }

  /**
   * Check the default path on the host for a JSON object describing the processor
   *
   * @return {Promise<URL>}
   */
  async byPath() {
    throw "Not implemented"
  }
  
  async _fetchPath() {
    return fetch(
        `https://${location.host}:${location.port}/${this.constructor.ID}`
    ).json();
  }

  /**
   * Find all mentions of a given processor in the body
   *
   * @return {Promise<Array<BodyProcessorInfo>>}
   */
  async byBody() {
    throw "Not implemented"
  }
}

const processorFinders = [];
