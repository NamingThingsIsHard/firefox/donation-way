async function findProcessor(method) {
  let results = await Promise.allSettled(
      processorFinders.map((finder) => finder[method]())
  );
  return results
      .filter(result => result.status === "fulfilled")
      .map(result => result.value)
}

async function main() {
  console.clear();
  console.log("calling main");
  let total = 0;
  let results = {};
  // TODO: Check DB if we have values for the website
  for (let method of ["bmyMeta", "byPath", "byBody"]) {
    try {
      let methodResults = await findProcessor(method);
      results[method] = methodResults;
      total += methodResults.length;
    } catch (e) {
    }
  }
  // TODO: check the body for processors
  browser.runtime.sendMessage({
    message: "total",
    value: total
  })

}

main().then(r => console.log("main done"))
