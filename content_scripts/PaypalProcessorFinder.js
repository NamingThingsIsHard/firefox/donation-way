/**
 *
 * @author ubuntu
 */
class PaypalProcessorFinder extends ProcessorFinder {
  async byMeta() {
    let node = this._getMetaTag();
    let ret = false;
    if (node) {
      ret = node.getAttribute("content");
    }
    return ret
  }

  async byBody() {
    console.log("Paypal.body")

    // Detect paypal buttons
    let $form = document.querySelector(`form[action="https://www.paypal.com/cgi-bin/webscr"`);

    if (!$form) {
      throw "No processors in body"
    }
    const $business = $form.querySelector(`input[type="hidden"][name="business"]`)
    if (!$business || $business.value.trim() === "") {
      throw "No business defined in paypal form"
    }
    const resultID = $business.value.trim();
    const nodeClass = `donation_${this.constructor.ID}-${resultID}`
    $form.classList.add(nodeClass)
    return {
      processorID: this.constructor.ID,
      results: [{
        resultID: resultID,
        node: `form[class="${nodeClass}"]`,
      }]
    }
  }

  _buildURL(config) {

  }
}

PaypalProcessorFinder.ID = "paypal";

processorFinders.push(new PaypalProcessorFinder());
