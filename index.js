browser.runtime.onMessage.addListener(function ({message, value}) {
  console.log(message, value)
  let total = value;
  if (total === 0) {
    browser.browserAction.disable()
    browser.browserAction.setBadgeText({text: ""})
    browser.browserAction.setBadgeBackgroundColor({
      color: "red"
    })
  } else {
    browser.browserAction.enable()
    browser.browserAction.setBadgeBackgroundColor({
      color: "green"
    })
    let text = `${total}`;
    browser.browserAction.setBadgeText({
      text: text
    })
  }
});
