# Donation way

Finds all the ways you can donate to a given website or entity on the page

Some websites really don't manage their donation options well!
You might've inadvertently blocked them using your adblocker,
 because ads are indeed pesky.
Or you stumbled upon some artist's homepage and they have (for some reason)
 hidden those donation links really well?
Maybe the site navigation was just so bad you never found the donation page?

Well, this extension aims to change that.
Hopefully, with increased adoption, the annoying, full-page dialogs asking for
 your monies can become a thing of the past,
It would also be great to discover that your favorite artist, organisation or
 group does **indeed** allow donations!
 
# Detected donation / payment methods

|   | Processor     | <meta> ID | /path    |
|---|---------------|-----------|----------|
| 🚧 | Donation page | donate    | /donate  |
| 🚧 | Flattr        | flattr    | /flattr  |
| 🚧 | Fundly        | fundly    | /fundly  |
| 🚧 | Patreon       | patreon   | /patreon |
| 🚧 | Paypal        | paypal    | /paypal  |
| 🚧 | SWIFT         | swift     | /swift   |
| 🚧 | Venmo         | venmo     | /venmo   |

# Detection methods

If you're a website owner or have a personal page, this might be interesting 
 for you. The precedence is in order of this listing.
 
**<meta>**

```html
<head>
    <!-- 
       The IDENTIFIER will the one of the above in the table.
       The content can end up being a simple string or JSON string.
       That will depend on the IDENTIFIER
    -->
    <meta name="donation:IDENTIFIER" content="DEPENDS ON IDENTIFIER" />
    <!-- An example -->
    <meta name="donation:SWIFT" content="A-SWIFT-ACCOUNT-ID" />
</head>
```

**A processor URL on the server**

As shown in the table above, the processor can detect the necessary values
 by attempting to contact the given URL and checking whether a JSON has been
 returned. 

For example, if `https://aurl.domain/donate` returns a well-formated JSON
 (format TBD) with a 200 HTTP, then the extension will know
 there's a donation page.
 
**A processor link on the page**

Links like these will be visually demarcated in the results to show 
 that they may be user content.
 
Unfortunately there's a lot of room for abuse here as any user will be able to
 add e.g a patreon link to their description.
A discussion will have to be had on how to fix this.